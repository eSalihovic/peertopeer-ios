//
//  ViewController.swift
//  iPhonesCanComunicate
//
//  Created by qsdbih on 2/8/18.
//  Copyright © 2018 es. All rights reserved.
//

import UIKit

class ColorSwitchViewController: UIViewController {
    
    @IBOutlet var connectionsLabel: UILabel!
    
    let colorService = ColorServiceManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorService.delegate = self
    }
    
    @IBAction func redTapped(_ sender: Any) {
        self.change(color: .red)
        colorService.send(colorName: "Red")
    }
    
    @IBAction func yellowTapped(_ sender: Any) {
        self.change(color: .yellow)
        colorService.send(colorName: "Yellow")
    }
    
    func change(color: UIColor) {
        UIView.animate(withDuration: 0.25) {
            self.view.backgroundColor = color
        }
    }
}

extension ColorSwitchViewController: ColorServiceManagerDelegate {
    
    func connectedDevicesChanged(manager: ColorServiceManager, connectedDevices: [String]) {
        OperationQueue.main.addOperation {
            self.connectionsLabel.text = "Connections: \(connectedDevices)"
        }
    }
    
    func colorChanged(manager: ColorServiceManager, colorString: String) {
        OperationQueue.main.addOperation {
            switch colorString {
            case "Red":
                self.change(color: .red)
            case "Yellow":
                self.change(color: .yellow)
            default:
                NSLog("%@", "Unknown color value received: \(colorString)")
            }
        }
    }
}
